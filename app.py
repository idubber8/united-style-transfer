from models import *
import constants

style_img = image_loader(constants.style_img).type(dtype)
content_img = image_loader(constants.content_img).type(dtype)
content2_img = image_loader(constants.content2_img).type(dtype)

cnn = models.vgg19(pretrained=True).features

if use_cuda:
    cnn = cnn.cuda()

content_layers_default = ['conv_4']
content2_layers_default = ['conv_4']
style_layers_default = ['conv_1', 'conv_2', 'conv_3', 'conv_4', 'conv_5']

input_img = content_img.clone()
output = run_style_transfer(cnn, content_img, style_img, content2_img, input_img)


imshow(output, constants.file_name)
